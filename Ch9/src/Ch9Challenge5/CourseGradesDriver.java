package Ch9Challenge5;

public class CourseGradesDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CourseGrades gradebook = new CourseGrades();
		
		GradedActivity lab1 = new GradedActivity();
		lab1.setScore(100);
		gradebook.setLab(lab1);
		
		PassFailExam passOrFail = new PassFailExam(20, 4, 70);
		gradebook.setPassFailExam(passOrFail);
		
		Essay essay = new Essay();
		essay.setScore(30, 20, 20, 30);
		gradebook.setEssay(essay);
		
		FinalExam finalTest = new FinalExam(70, 10);
		gradebook.setFinalExam(finalTest);
		
		System.out.println(gradebook);
	}

}
