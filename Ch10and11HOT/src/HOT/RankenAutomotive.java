package HOT;

import java.awt.BorderLayout;
import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class RankenAutomotive extends Application{

	private Label oilLabel = new Label("Oil Change: $26");
	private Label lubeLabel = new Label("Lube Job: $18");
	private Label radiatorLabel = new Label("Radiator Flush: $30");
	private Label transmissionLabel = new Label("Transmision Flush: $80");
	private Label inspectionLabel = new Label("Inspection: $15");
	private Label mufflerLabel = new Label("Muffler Replacement: $100");
	private Label tireLabel = new Label("Tire Rotation: $20");
	private Label routeLabel = new Label("Routine Services");
	private Label nonRouteLabel = new Label("Nonroutine Services");
	private Label partsLabel = new Label("Parts Charges:");
	private Label hoursLabel = new Label("Hours of Labor");
	private CheckBox oilChange = new CheckBox();
	private CheckBox lubeJob = new CheckBox();
	private CheckBox radiatorFlush = new CheckBox();
	private CheckBox transmissionFlush = new CheckBox();
	private CheckBox inspectionCheck = new CheckBox();
	private CheckBox mufflerReplacement = new CheckBox();
	private CheckBox tireRotation = new CheckBox();
	private TextField partsInput = new TextField();
	private TextField hoursInput = new TextField();
	private Button calculateCostBtn = new Button("Calculate Cost");
	private Button exitBtn = new Button("Exit");
	private Label totalCost = new Label();
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		HBox oil = new HBox(5, oilChange, oilLabel);
		HBox lube = new HBox(5, lubeJob, lubeLabel);
		HBox radiator = new HBox(5, radiatorFlush, radiatorLabel);
		HBox transmission = new HBox(5, transmissionFlush, transmissionLabel);
		HBox inspection = new HBox(5, inspectionCheck, inspectionLabel);
		HBox muffler = new HBox(5, mufflerReplacement, mufflerLabel);
		HBox tire = new HBox(5, tireRotation, tireLabel);
		HBox hours = new HBox(5, hoursLabel, hoursInput);
		HBox parts = new HBox(5, partsLabel, partsInput);
		HBox buttons = new HBox(5, calculateCostBtn, exitBtn);
		calculateCostBtn.setOnAction(new ButtonClickHandler());
		exitBtn.setOnAction(new ProgramCloseHandler());
		VBox StageDisplay = new VBox(10, routeLabel, oil, lube, radiator, transmission, inspection, muffler, tire, nonRouteLabel, parts, hours, totalCost, buttons);
		StageDisplay.setAlignment(Pos.CENTER_LEFT);
		Scene scene = new Scene(StageDisplay, 400, 400);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Ranken's Automotive Maintenance");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			int total = 0;
			
			if(oilChange.isSelected() == true) {
				total += 26;
			}
			if(lubeJob.isSelected() == true) {
				total += 18;
			}
			if(radiatorFlush.isSelected() == true) {
				total += 30;
			}
			if(transmissionFlush.isSelected() == true) {
				total += 80;
			}
			if(inspectionCheck.isSelected() == true) {
				total += 15;
			}
			if(mufflerReplacement.isSelected() == true) {
				total += 100;
			}
			if(tireRotation.isSelected() == true) {
				total += 20;
			}
			
			System.out.println(hoursInput.getText());
			
			if(partsInput.getText().isEmpty() && hoursInput.getText().isEmpty()) {
				totalCost.setText("Total Charges: $" + total);
			}
			else if(partsInput.getText() != "" && hoursInput.getText().isEmpty()){
				try {
					double partsCost = Double.parseDouble(partsInput.getText());
					total += (partsCost);
					totalCost.setText("Total Charges: $" + total);
				}
				catch(NumberFormatException e){
					totalCost.setText("Error, invalid entry for parts/hours");
				}
				
				totalCost.setText("Total Charges: $" + total);
			}
			else if(partsInput.getText().isEmpty() && hoursInput.getText() != "") {
				try {
					double numHours = Double.parseDouble(hoursInput.getText());
					total += (numHours * 20);
					totalCost.setText("Total Charges: $" + total);
				}
				catch(NumberFormatException e){
					totalCost.setText("Error, invalid entry for parts/hours");
				}
			}
			else {
				try {
					double partsCost = Double.parseDouble(partsInput.getText());
					double numHours = Double.parseDouble(hoursInput.getText());
					
					total += (partsCost + (numHours * 20));
					totalCost.setText("Total Charges: $" + total);
				}
				catch(NumberFormatException e){
					totalCost.setText("Error, invalid entry for parts/hours");
				}
			}
			
			
		}
	}
	
	class ProgramCloseHandler implements EventHandler<ActionEvent> {
		public ProgramCloseHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
		    Stage stage = (Stage) exitBtn.getScene().getWindow();
		    stage.close();
		}
	}

}
