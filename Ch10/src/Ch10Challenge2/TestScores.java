package Ch10Challenge2;

public class TestScores {
	private int[] testScores;
	
	public TestScores(int[] scores) {
		testScores = scores;
	}
	
	public double getAverage() throws InvalidTestScores {
		int totalScores = 0;
		for(int x = 0; x < testScores.length; ++x) {
			totalScores += testScores[x];
		}
		double avgScores = (double)totalScores / (double)testScores.length;
		
		if(avgScores < 100) {
			throw new InvalidTestScores();
		}
		else {
			return avgScores;
		}
		
	}
	
	
}
