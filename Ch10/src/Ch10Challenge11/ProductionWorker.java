package Ch10Challenge11;

public class ProductionWorker extends Employee {
	private int shift;
	private double payRate;
	public int DAY_SHIFT = 1;
	public int NIGHT_SHIFT = 2;
	
	public ProductionWorker(String n, String num, String date, int sh, double rate) throws InvalidEmployeeNumber, InvalidShift, InvalidPayRate {
		super(n, num, date);
		if(sh != 1 && sh != 2) {
			throw new InvalidShift();
		}else {
			shift = sh;	
		}
		if(rate < 0) {
			throw new InvalidPayRate();
		}else {
			payRate = rate;	
		}
	}
	
	public ProductionWorker() {
		
	}
	
	public void setShift(int s) {
		shift = s;
	}
	
	public void setPayRate(double p) {
		payRate = p;
	}
	
	public int getShift() {
		return shift;
	}
	
	public double getPayRate() {
		return payRate;
	}
	
	public String toString() {
		return "Employee #" + (super.getEmployeeNumber()) + ": " + super.getName() + "\nHired: " + super.getHireDate() + "\nShift: " + getShift() + "\nPay Rate: " + getPayRate();
	}
}
