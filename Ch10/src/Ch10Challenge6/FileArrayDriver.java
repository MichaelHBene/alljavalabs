package Ch10Challenge6;

import java.io.IOException;

public class FileArrayDriver {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		int[] numberArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		FileArray.writeArray("src\\Ch10Challenge6\\FileArray.txt", numberArray);
		FileArray.readArray("src\\Ch10Challenge6\\FileArray.txt", numberArray);
		
		
	}

}
