import java.util.Scanner;
public class Challenge10
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Please the score for Test #1: ");
        double test1 = keyboard.nextDouble();
        System.out.println("Please the score for Test #2: ");
        double test2 = keyboard.nextDouble();
        System.out.println("Please the score for Test #3: ");
        double test3 = keyboard.nextDouble();
        double testAvg = (test1 + test2 + test3) / 3;
        
        System.out.println("Test Scores and Average:\n--------------------");
        System.out.println("Test #1: " + test1 + "%");
        System.out.println("Test #2: " + test2 + "%");
        System.out.println("Test #3: " + test3 + "%");
        System.out.println("Average Score: " + testAvg + "%");
    }
}