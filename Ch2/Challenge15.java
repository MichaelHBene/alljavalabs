import java.util.Scanner;
public class Challenge15
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Enter the amount of Cookies you would like to bake:");
        int numCookies = keyboard.nextInt();
        int cookiesPerBatch = 48;
        double sugarPerBatch = 1.5;
        int butterPerBatch = 1;
        double flourPerBatch = 2.75;
        double numBatches = numCookies / cookiesPerBatch;
        double numSugar = sugarPerBatch * numBatches;
        double numButter = butterPerBatch * numBatches;
        double numFlour = flourPerBatch * numBatches;
        System.out.println("To make " + numCookies + " cookies, you will need the following:\n   " + numSugar + " cups of Sugar,\n   " + numButter + " cups of Butter,\n   and " + numFlour + " cups of Flour.");
        
        
    }
}