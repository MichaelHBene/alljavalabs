package HOT;

public class HOT3Class {
	public static int addNumbers(int num1, int num2) {
		int result = num1 + num2;
		return result;
	}
	
	public static int addNumbers(int num1, int num2, int num3) {
		int result = num1 + num2 + num3;
		return result;
	}
}
