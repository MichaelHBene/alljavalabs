package HOT;

public class Book {

	private String name;
	private String date;
	private String author;
	
	public Book(String nameInput, String dateInput, String authorInput) {
		name = nameInput;
		date = dateInput;
		author = authorInput;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getAuthor() {
		return author;
	}
	
	
}
