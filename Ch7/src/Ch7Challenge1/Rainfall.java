package Ch7Challenge1;

public class Rainfall {
	private double[] rainfall = new double[12];
	
	public void setRainfall(double numRain, int count) {
		rainfall[count] = numRain;
	}
	
	public double getTotal() {
		double total = 0; 
		for (int i = 0; i < rainfall.length; i++) {
		     total += rainfall[i];	
		}
		return total;

	}
	
	public double getAvg() {
		double total = 0; 
		double average; 
		for (int i = 0; i < rainfall.length; i++) {
			   total += rainfall[i];	
		}
		average = total / rainfall.length;
		return average;
	}
	
	public int getHighest() {
		double highest = rainfall[0];
		int highestLoc = -1;
		for (int i = 1; i < rainfall.length; i++)
		{
			if (rainfall[i] > highest) {
				highestLoc = i;
			    highest = rainfall[i];	
			}
		}
		
		return highestLoc + 1;

	}
	
	public int getLowest() {
		double lowest = rainfall[0];
		int lowestLoc = -1;
		for (int i = 1; i < rainfall.length; i++)
		{
			if (rainfall[i] < lowest) {
				lowestLoc = i;
			    lowest = rainfall[i];	
			}
		}
		
		return lowestLoc + 1;

	}

}
