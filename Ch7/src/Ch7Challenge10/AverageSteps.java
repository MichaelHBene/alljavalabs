package Ch7Challenge10;

import java.io.*;
import java.util.Scanner;

public class AverageSteps {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		File inputFile = new File("src\\Ch7Challenge10\\steps.txt");
		Scanner steps = new Scanner(inputFile);
		
		int year[][] = new int[12][];
		
		year[0] = new int[31];
		year[1] = new int[28];
		year[2] = new int[31];
		year[3] = new int[30];
		year[4] = new int[31];
		year[5] = new int[30];
		year[6] = new int[31];
		year[7] = new int[31];
		year[8] = new int[30];
		year[9] = new int[31];
		year[10] = new int[30];
		year[11] = new int[31];
		
		//Sets ragged array with daily steps for each month
		for(int x = 0; x < year.length; ++x) {
			for (int i = 0; i < year[x].length; ++i) {
				year[x][i] = steps.nextInt();
				/*
				System.out.println("Month: " + (x + 1) + " Day: " + (i + 1));
				System.out.println(year[x][i]);
				*/
			}
		}
		
		//Get average for January
		int janTotal = 0;
		for(int x = 0; x < year[0].length; ++x) {
			janTotal += year[0][x];
		}
		int janAvg = janTotal / year[0].length;
		
		//Get average for February
		int febTotal = 0;
		for(int x = 0; x < year[1].length; ++x) {
			febTotal += year[1][x];
		}
		int febAvg = febTotal / year[1].length;
		
		//Get average for March
		int marTotal = 0;
		for(int x = 0; x < year[2].length; ++x) {
			marTotal += year[2][x];
		}
		int marAvg = janTotal / year[2].length;
		
		//Get average for April
		int aprTotal = 0;
		for(int x = 0; x < year[3].length; ++x) {
			aprTotal += year[3][x];
		}
		int aprAvg = aprTotal / year[3].length;
		
		//Get average for May
		int mayTotal = 0;
		for(int x = 0; x < year[4].length; ++x) {
			mayTotal += year[4][x];
		}
		int mayAvg = mayTotal / year[4].length;
		
		//Get average for June
		int junTotal = 0;
		for(int x = 0; x < year[5].length; ++x) {
			junTotal += year[5][x];
		}
		int junAvg = junTotal / year[5].length;
		
		//Get average for July
		int julTotal = 0;
		for(int x = 0; x < year[6].length; ++x) {
			julTotal += year[6][x];
		}
		int julAvg = julTotal / year[6].length;
		
		//Get average for August
		int augTotal = 0;
		for(int x = 0; x < year[7].length; ++x) {
			augTotal += year[7][x];
		}
		int augAvg = augTotal / year[7].length;
		
		//Get average for September
		int sepTotal = 0;
		for(int x = 0; x < year[8].length; ++x) {
			sepTotal += year[8][x];
		}
		int sepAvg = sepTotal / year[8].length;
		
		//Get average for October
		int octTotal = 0;
		for(int x = 0; x < year[9].length; ++x) {
			octTotal += year[9][x];
		}
		int octAvg = octTotal / year[9].length;
		
		//Get average for November
		int novTotal = 0;
		for(int x = 0; x < year[10].length; ++x) {
			novTotal += year[10][x];
		}
		int novAvg = novTotal / year[10].length;
		
		//Get average for December
		int decTotal = 0;
		for(int x = 0; x < year[11].length; ++x) {
			decTotal += year[11][x];
		}
		int decAvg = decTotal / year[11].length;
		
		System.out.println("January Average: " + janAvg + " steps.");
		System.out.println("February Average: " + febAvg + " steps.");
		System.out.println("March Average: " + marAvg + " steps.");
		System.out.println("April Average: " + aprAvg + " steps.");
		System.out.println("May Average: " + mayAvg + " steps.");
		System.out.println("June Average: " + junAvg + " steps.");
		System.out.println("July Average: " + julAvg + " steps.");
		System.out.println("August Average: " + augAvg + " steps.");
		System.out.println("September Average: " + sepAvg + " steps.");
		System.out.println("October Average: " + octAvg + " steps.");
		System.out.println("November Average: " + novAvg + " steps.");
		System.out.println("December Average: " + decAvg + " steps.");
		
	}

}
