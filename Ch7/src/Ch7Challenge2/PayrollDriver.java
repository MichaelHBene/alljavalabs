package Ch7Challenge2;

import java.util.Scanner;
public class PayrollDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Welcome to Payroll Portal:");
		
		Payroll paystubs = new Payroll();
		
		int[] employeeID = new int[7];
		double[] rateInput = new double[7];
		int[] hoursInput = new int[7];
		
		for(int x = 0; x < employeeID.length; ++x) {
			System.out.println("Enter the hours worked for employee: " + paystubs.getId(x));
			int hourInput = keyboard.nextInt();
			while(hourInput < 0) {
				System.out.println("Error, cannot work negative hours, please enter hours again.");
				hourInput = keyboard.nextInt();
			}
			hoursInput[x] = hourInput;
			System.out.println("Enter the pay rate for employee: " + paystubs.getId(x));
			double payInput = keyboard.nextDouble();
			while(payInput < 6) {
				System.out.println("Error, Pay rate must be 6 or higher, please enter rate again.");
				payInput = keyboard.nextDouble();
			}
			rateInput[x] = payInput;
		}
		
		paystubs.setHours(hoursInput);
		paystubs.setPayRate(rateInput);
		paystubs.setWages();
		
		for(int x = 0; x < employeeID.length; ++x) {
			System.out.println(paystubs.getWages(x));
			System.out.println("");
		}
		
		System.out.println("Thank you for using the Payroll Portal.");
		
		
	}

}
