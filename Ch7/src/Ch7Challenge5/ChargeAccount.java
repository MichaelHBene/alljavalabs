package Ch7Challenge5;

import java.util.Scanner;
import java.io.*;
public class ChargeAccount {
	private int[] accountNumbers = new int[18];
	
	public ChargeAccount() throws FileNotFoundException{
		File file = new File("src\\Ch7Challenge5\\numbers.txt");
		Scanner inputFile = new Scanner(file);	

		for (int x = 0; x < accountNumbers.length; ++x) {
			accountNumbers[x] = inputFile.nextInt();
		}
	}
	public boolean getChargeValid(int accountNumber) {
		boolean isValid = false;
		
		for(int x = 0; x < accountNumbers.length; ++x) {
			if(accountNumber == accountNumbers[x]) {
				isValid = true;
			}
		}
		
		return isValid;
		
	}
}
