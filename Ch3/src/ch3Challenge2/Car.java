package ch3Challenge2;

public class Car {
	private int yearModel;
	private String make;
	private int speed;
	
	public Car(int year, String carMake) {
		yearModel = year;
		make = carMake;
		speed = 0;
	}
	
	public int getYear() {
		return yearModel;
	}
	
	public String getMake() {
		return make;
	}
	
	public int getSpeed() {
		return speed;
	}
	
	public int accelerate() {
		speed +=5;
		return speed;
	}
	
	public int brake() {
		speed-=5;
		return speed;
	}
}
