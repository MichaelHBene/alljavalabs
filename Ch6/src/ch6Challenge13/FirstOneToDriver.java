package ch6Challenge13;

public class FirstOneToDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Player player1 = new Player(50, "Craig");
		Player player2 = new Player(50, "Second Craig");
		
		Die playDie = new Die(6);
		
		while (player1.getPoints() != 1 && player2.getPoints() != 1) {
			playDie.roll();
			if((player1.getPoints() - playDie.getValue()) < 0) {
				player1.addPoints(playDie.getValue());
				System.out.println(player1.getName() + "'s score: " + player1.getPoints());
			}
			else {
				player1.subtractPoints(playDie.getValue());
				System.out.println(player1.getName() + "'s score: " + player1.getPoints());
			}
			
			playDie.roll();
			if((player2.getPoints() - playDie.getValue()) < 0) {
				player2.addPoints(playDie.getValue());
				System.out.println(player2.getName() + "'s score: " + player2.getPoints());
			}
			else {
				player2.subtractPoints(playDie.getValue());
				System.out.println(player2.getName() + "'s score: " + player2.getPoints());
			}
			
		}
		
		if(player1.getPoints() == 1) {
			System.out.println(player1.getName() + " wins!");
		}
		
		else if(player2.getPoints() == 1) {
			System.out.println(player2.getName() + " wins!");
		}
		
		
		
	}

}
