package ch6Challenge3;

import java.util.Scanner;
public class CarpetCalculatorDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the length of the room: ");
		double roomLength = keyboard.nextDouble();
		System.out.println("Please enter the Width of the room: ");
		double roomWidth = keyboard.nextDouble();
		System.out.println("Please enter the price of the carpet per square foot: ");
		double carpetCost = keyboard.nextDouble();
		
		RoomDimension room1 = new RoomDimension(roomLength, roomWidth);
		RoomCarpet carpet1 = new RoomCarpet (room1, carpetCost);
		
		System.out.println(room1.toString());
		System.out.println(carpet1.toString());
		
		
		
		
	}

}
