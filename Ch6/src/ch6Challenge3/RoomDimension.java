package ch6Challenge3;

public class RoomDimension {
	private double length;
	private double width;
	
	public RoomDimension(double roomLength, double roomWidth) {
		length = roomLength;
		width = roomWidth;
	}
	
	public double getArea() {
		double area = length * width;
		return area;
	}
	
	public String toString() {
		String dimensionString = "The area of the room is " + this.getArea();
		return dimensionString;
	}
	
}
