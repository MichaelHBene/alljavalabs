package ch6Challenge3;

public class RoomCarpet {
	private RoomDimension size;
	private double carpetCost;
	
	public RoomCarpet(RoomDimension dimension, double cost) {
		size = dimension;
		carpetCost = cost;
	}
	
	public double getTotalCost() {
		double totalCost = carpetCost * (size.getArea());
		return totalCost;
	}
	
	public String toString() {
		String carpetString = "The total cost of the carpet is: " + this.getTotalCost();
		return carpetString;
	}
	
}