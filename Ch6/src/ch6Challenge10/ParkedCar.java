package ch6Challenge10;

public class ParkedCar {
	private String make;
	private String model;
	private String color;
	private String license;
	private int parkedMinutes;
	
	public ParkedCar(String carMake, String carModel, String carColor, int parked, String licensePlate) {
		make = carMake;
		model = carModel;
		color = carColor;
		parkedMinutes = parked;
		license = licensePlate;
	}
	
	public String getMake() {
		return make;
	}
	public String getModel() {
		return model;
	}
	public String getColor() {
		return color;
	}
	public String getLicense() {
		return license;
	}
	public int getMinutes() {
		return parkedMinutes;
	}
}
