package ch6Challenge10;

public class ParkingMeter {
	private int minutesPurchased;
	
	public ParkingMeter(int numMinutes) {
		minutesPurchased = numMinutes;
	}
	
	public int getMinutes() {
		return minutesPurchased;
	}
}
