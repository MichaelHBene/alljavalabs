package ch6Challenge14;

public class Player {
	private int points;
	private String name;
	
	public Player(int startingPoints, String playerName) {
		points = startingPoints;
		name = playerName;
	}
	
	public void addPoints(int earned) {
		points += earned;
	}
	
	public void subtractPoints(int lost) {
		points -= lost;
	}
	
	public int getPoints() {
		return points;
	}
	public String getName() {
		return name;
	}

}
