package ch6Challenge2;

public class InventoryItem {
	
	private String description;
	private int units;
	
	public InventoryItem(String desc, int unit) {
		description = desc;
		units = unit;
	}
	
	public InventoryItem(InventoryItem item2) {
		description = item2.description;
		units = item2.units;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getUnits() {
		return units;
	}

}
