package ch6Challenge11;

import java.util.Scanner;
public class GeometryDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Geometry Calculator\n===================");
		System.out.println("1: Calculate the Area of a Circle");
		System.out.println("2: Calculate the Area of a Rectangle");
		System.out.println("3: Calculate the Area of a Triangle");
		System.out.println("4: Quit");
		System.out.println("Enter your choice (1-4)");
		int userChoice = keyboard.nextInt();
		
		Geometry shape = new Geometry();
		
		switch(userChoice) {
			case 1:
				System.out.println("Please enter the radius of the circle:");
				double radius = keyboard.nextDouble();
				System.out.println("The area is: " + shape.getArea(radius));
				break;
			case 2:
				System.out.println("Please enter the width of the rectangle:");
				int width = keyboard.nextInt();
				System.out.println("Please enter the length of the rectangle:");
				int length = keyboard.nextInt();
				System.out.println("The area is: " + shape.getArea(width, length));
				break;
			case 3:
				System.out.println("Please enter the base of the rectangle:");
				double base = keyboard.nextDouble();
				System.out.println("Please enter the height of the rectangle:");
				double height = keyboard.nextDouble();
				System.out.println("The area is: " + shape.getArea(base, height));
				break;
			case 4:
				System.out.println("Thanks for using the Geometry Calculator!");
				break;
			default:
				System.out.println("Error. Invalid Input.");
				break;
				
		}
		
		
	}

}
