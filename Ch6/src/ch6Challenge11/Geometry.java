package ch6Challenge11;

public class Geometry {

	public static double getArea(double radius) {
		
		if(radius < 0) {
			System.out.println("Error. Radius cannot be negative.");
			return 0;
		}
		else {
			double area = Math.PI * (radius * radius);
			return area;	
		}
	}
	public static int getArea(int width, int length) {
		if(width < 0 || length < 0) {
			System.out.println("Error. Radius cannot be negative.");
			return 0;
		}
		else {
			int area = width * length;
			return area;	
		}
	}
	public static double getArea(double base, double height) {
		if(base < 0 || height < 0) {
			System.out.println("Error. Radius cannot be negative.");
			return 0;
		}
		else {
			double area = base * height * .5;
			return area;	
		}
	}
}
