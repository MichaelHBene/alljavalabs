package ch5Challenge8;

import java.util.Scanner;
public class GreatestAndLeast {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Please enter a number or -99 to end: ");
		
		int number = keyboard.nextInt();
		int greatest = number;
		int lowest = number;
		
		while(number != -99) {
			if(number > greatest) {
				greatest = number;
			}
			if(number < lowest) {
				lowest = number;
			}
			
			System.out.println("Please enter a number or -99 to end: ");
			number = keyboard.nextInt();
		}
		
		System.out.println("The greatest number entered was: " + greatest);
		System.out.println("The lowest number entered was: " + lowest);
	}

}
