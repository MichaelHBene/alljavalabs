package ch5Challenge3;

public class DistanceFile {
	private double speed;
	private int time;
	
	public DistanceFile(double speedInput, int timeInput) {
		speed = speedInput;
		time = timeInput;
	}
	
	public void setTime(int timeInput) {
		time = timeInput;
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public int getTime() {
		return time;
	}
	
	public double getDistance() {
		double distance;
		distance = speed * time;
		return distance;
	}
}
