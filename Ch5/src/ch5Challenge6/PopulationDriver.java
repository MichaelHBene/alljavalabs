package ch5Challenge6;

import java.util.Scanner;
public class PopulationDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Please enter the starting population:");
		
		int population = keyboard.nextInt();
		
		if(population >= 2) {
			System.out.println("Please enter the average daily population increase:");
			double avgIncrease = keyboard.nextDouble();
			if(avgIncrease >= 0) {
				System.out.println("Please enter the number of days the colony will populate:");
				int days = keyboard.nextInt();
				if(days >= 1) {
					Population colony = new Population(population, avgIncrease, days);
					
					colony.calcPopulation();
				}
				else {
					System.out.println("Error. Number of days cannot be less than 1.");
				}
			}
			else {
				System.out.println("Error. Average increase cannot be a negative number.");
			}
		}
		else {
			System.out.println("Error. Starting population cannot be less than 2.");
		}
	}

}
