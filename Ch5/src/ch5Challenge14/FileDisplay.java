package ch5Challenge14;
import java.io.*;
import java.util.Scanner;

public class FileDisplay {
	private File file;
	private Scanner fileScanner;
	
	public FileDisplay(String nameInput)throws IOException {
		file = new File("src\\ch5Challenge14\\" + nameInput + ".txt");
		fileScanner = new Scanner(file);
	}
	
	public void displayHead() {

		for(int x = 1; x <= 5 && fileScanner.hasNext(); ++x) {
			System.out.println(fileScanner.nextLine());
		}	
		
	}
	
	public void displayContents() {
		while(fileScanner.hasNext()) {
			String nextLine = fileScanner.nextLine();
			System.out.println(nextLine);
		}
	}
	
	public void displayWithLineNumbers() {
		int lineCounter = 1;
		while(fileScanner.hasNext()) {
			System.out.println(lineCounter + ": " + fileScanner.nextLine());
			++lineCounter;
		}
	}
	
	
}
