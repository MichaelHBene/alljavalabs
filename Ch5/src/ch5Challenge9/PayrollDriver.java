package ch5Challenge9;

import java.util.Scanner;
public class PayrollDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		boolean isFirst = true;
		System.out.println("Please enter an employee ID number, or 0 to end: ");
		int empID = keyboard.nextInt();
		
		if(empID >= 0) {
			double grossPay = 0;
			double stateTax = 0;
			double fedTax = 0;
			double FICA = 0;
			double netPay = 0;
			double totalGrossPay = 0;
			double totalStateTax = 0;
			double totalFedTax = 0;
			double totalFICA = 0;
			double totalNetPay = 0;
			
			while(empID != 0) {
				if(isFirst == false) {
					System.out.println("\nPlease enter an employee ID number, or 0 to end: ");
					empID = keyboard.nextInt();
					if(empID > 0) {
						System.out.println("Please enter the employee's gross pay: ");
						grossPay = keyboard.nextDouble();
						if(grossPay >= 0) {
							System.out.println("Please enter the employee's state tax: ");
							stateTax = keyboard.nextDouble();
							if(stateTax >= 0 && stateTax < grossPay) {
								System.out.println("Please enter the employee's federal tax: ");
								fedTax = keyboard.nextDouble();
								if(fedTax >= 0 && fedTax < grossPay) {
									System.out.println("Please enter the employee's FICA withholdings: ");
									FICA = keyboard.nextDouble();
									if(FICA >= 0 && FICA < grossPay) {
										if(stateTax + fedTax + FICA > grossPay) {
											System.out.println("Error. Taxes cannot be greater than the employee's gross pay. Please reenter user Data.");
										}
										else {
											Payroll employee = new Payroll(empID, grossPay, stateTax, fedTax, FICA);
											netPay = employee.getNetPay();
											
											totalGrossPay += grossPay;
											totalStateTax += stateTax;
											totalFedTax += fedTax;
											totalFICA += FICA;
											totalNetPay += netPay;
											System.out.printf("The employee's net pay is: $%.2f", netPay);
										}
									}
									else {
										System.out.println("Error. FICA withholdings cannot be negative / greater than the gross pay.");
									}
									
								}
								else {
									System.out.println("Error. Federal tax cannot be negative / greater than the gross pay.");
								}
								
							}
							else {
								System.out.println("Error. State tax cannot be negative / greater than the gross pay.");
							}
							
						}
						else {
							System.out.println("Error. Gross pay cannot be negative.");
						}
					}
					else if(empID == 0) {
					}
					else {
						System.out.println("Error. employee ID cannot be less than 0.");
						break;
					}
				}
				else {
					if(empID >= 0) {
						isFirst = false;
						if(empID > 0) {
							System.out.println("\nPlease enter the employee's gross pay: ");
							grossPay = keyboard.nextDouble();
							if(grossPay >= 0) {
								System.out.println("Please enter the employee's state tax: ");
								stateTax = keyboard.nextDouble();
								if(stateTax >= 0 && stateTax < grossPay) {
									System.out.println("Please enter the employee's federal tax: ");
									fedTax = keyboard.nextDouble();
									if(fedTax >= 0 && fedTax < grossPay) {
										System.out.println("Please enter the employee's FICA withholdings: ");
										FICA = keyboard.nextDouble();
										if(FICA >= 0 && FICA < grossPay) {
											if(stateTax + fedTax + FICA > grossPay) {
												System.out.println("Error. Taxes cannot be greater than the employee's gross pay. Please reenter user Data.");
											}
											else {
												Payroll employee = new Payroll(empID, grossPay, stateTax, fedTax, FICA);
												netPay = employee.getNetPay();
												totalGrossPay += grossPay;
												totalStateTax += stateTax;
												totalFedTax += fedTax;
												totalFICA += FICA;
												totalNetPay += netPay;
												
												System.out.printf("The employee's net pay is: $%.2f", netPay);
											}
										}
										else {
											System.out.println("Error. FICA withholdings cannot be negative / greater than the gross pay.");
										}
									
									}
									else {
										System.out.println("Error. Federal tax cannot be negative / greater than the gross pay.");
									}
								
								}
								else {
									System.out.println("Error. State tax cannot be negative / greater than the gross pay.");
								}
							
							}
							else {
								System.out.println("Error. Gross pay cannot be negative.");
							}
						}
						else {
							System.out.println("Error. employee ID cannot be less than 0.");
							break;
						}
					}
				}
			}
			System.out.println("Thank you for using the Payroll Calculator.");
			System.out.printf("The total gross pay for employees was: $%.2f\n", totalGrossPay);
			System.out.printf("The total state tax for employees was: $%.2f\n",totalStateTax);
			System.out.printf("The total federal tax for employees was: $%.2f\n", totalFedTax);
			System.out.printf("The total FICA withholdings for employees was: $%.2f\n", totalFICA);
			System.out.printf("The total net pay for employees was: $%.2f\n", totalNetPay);
			
		}
		else {
			System.out.println("Error. employee ID cannot be less than 0.");
		}
	}

}
