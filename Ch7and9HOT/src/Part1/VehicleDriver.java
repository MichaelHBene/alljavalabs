package Part1;

public class VehicleDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Creates a Vehicle reference to the Car class using polymorphism
		Vehicle car = new Car();
		//Creates a Vehicle reference to the Truck class using polymorphism
		Vehicle truck = new Truck();
		
		//Calls the overrided accelerate() method from the Car class and prints
		car.accelerate();
		System.out.println(car.getSpeed());
		//Calls the overrided accelerate() method from the Truck class and prints
		truck.accelerate();
		System.out.println(truck.getSpeed());
		
	}

}
