package Part1;

public class Game {
	//String which holds the title of the game.
	private String game;
	//Int which holds the max amount of players in the game.
	private int maxPlayers;
	
	//Setter for the game field
	public void setGame(String title) {
		game = title;
	}
	
	//Setter for the maxPlayers field
	public void setMaxPlayers(int max) {
		maxPlayers = max;
	}
	
	//Getter for the game field
	public String getGame() {
		return game;
	}
	
	//Getter for the maxPlayers field
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	//Overrides the object class' toString method to display the game object's fields
	public String toString() {
		return "You are currently playing:\n" + getGame() + "\nMax Players: " + getMaxPlayers();
	}
}
