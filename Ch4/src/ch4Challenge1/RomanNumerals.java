package ch4Challenge1;

import java.util.Scanner;
public class RomanNumerals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Please enter a number 1-10:");
		Scanner keyboard = new Scanner (System.in);
		int numberInput = keyboard.nextInt();
		System.out.println("Your roman numeral conversion is:");
		if(numberInput == 1) {
			System.out.println("I");
		}
		else if(numberInput == 2) {
			System.out.println("II");
		}
		else if(numberInput == 3) {
			System.out.println("III");
		}
		else if(numberInput == 4) {
			System.out.println("IV");
		}
		else if(numberInput == 5) {
			System.out.println("V");
		}
		else if(numberInput == 6) {
			System.out.println("VI");
		}
		else if(numberInput == 7) {
			System.out.println("VII");
		}
		else if(numberInput == 8) {
			System.out.println("VIII");
		}
		else if(numberInput == 9) {
			System.out.println("IX");
		}
		else if(numberInput == 10) {
			System.out.println("X");
		}
		else{
			System.out.println("The number you entered is not valid.");
		}
		

	}

}
