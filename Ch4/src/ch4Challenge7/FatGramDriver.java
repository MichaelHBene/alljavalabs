package ch4Challenge7;

import java.util.Scanner;
public class FatGramDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		FatGram foodItem = new FatGram();
		System.out.println("Please enter the number of calories:");
		int numCalories = keyboard.nextInt();
		foodItem.setCal(numCalories);
		System.out.println("Please enter the number of fat grams:");
		int numFats = keyboard.nextInt();
		foodItem.setFat(numFats);
		
		System.out.println(foodItem.calcCalFromFat());
		
		System.out.println(foodItem.calcCalPercent());
		
		
		
	}

}
