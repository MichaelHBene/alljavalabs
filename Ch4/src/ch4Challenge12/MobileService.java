package ch4Challenge12;

public class MobileService {
	private String mobilePackage;
	private int minutes;
	
	public void setPackage(String packageChoice) {
		mobilePackage = packageChoice;
	}
	
	public void setMinutes(int minutesInput) {
		minutes = minutesInput;
	}
	
	public String getCharge() {
		double minutesCost = 0;
		double aCharge = this.getACharge();
		double bCharge = this.getBCharge();
		double cCharge = this.getCCharge();
		String chargeString = "";
		
		if(mobilePackage.equalsIgnoreCase("a")) {
			chargeString = "Your chosen mobile package will cost: $" + aCharge + ".";
			if(aCharge > bCharge) {
				chargeString += "\nIf you had chosen package B, you could have saved $" + (aCharge - bCharge);
			}
			if(aCharge > cCharge) {
				chargeString += "\nIf you had chosen package C, you could have saved $" + (aCharge - cCharge);
			}
		}
		else if(mobilePackage.equalsIgnoreCase("b")) {
			chargeString = "Your chosen mobile package will cost: $" + bCharge + ".";
			if(bCharge > aCharge) {
				chargeString += "\nIf you had chosen package A, you could have saved $" + (bCharge - aCharge);
			}
			if(bCharge > cCharge) {
				chargeString += "\nIf you had chosen package C, you could have saved $" + (bCharge - cCharge);
			}
		}
		else if(mobilePackage.equalsIgnoreCase("c")) {
			chargeString = "Your chosen mobile package will cost: $" + cCharge + ".";
			if(cCharge > aCharge) {
				chargeString += "\nIf you had chosen package A, you could have saved $" + (cCharge - aCharge);
			}
			if(cCharge > bCharge) {
				chargeString += "\nIf you had chosen package B, you could have saved $" + (cCharge - bCharge);
			}
		}
		return chargeString;
	}
	
	public double getACharge() {
		double charge = 0;
		double minutesCost = 0;
		if(minutes > 450) {
			minutesCost = (double)(minutes - 450) * .45;
		}
		charge = 39.99 + minutesCost;
		
		return charge;
	}
	public double getBCharge() {
		double charge = 0;
		double minutesCost = 0;
		if(minutes > 900) {
			minutesCost = (double)(minutes - 900) * .40;
		}
		charge = 59.99 + minutesCost;
		
		return charge;
	}
	public double getCCharge() {
		double charge = 0;
		charge = 69.99;
		
		return charge;
	}
}
