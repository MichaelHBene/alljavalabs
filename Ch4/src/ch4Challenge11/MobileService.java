package ch4Challenge11;

public class MobileService {
	private String mobilePackage;
	private int minutes;
	
	public void setPackage(String packageChoice) {
		mobilePackage = packageChoice;
	}
	
	public void setMinutes(int minutesInput) {
		minutes = minutesInput;
	}
	
	public String getCharge() {
		double charge = 0;
		double minutesCost = 0;
		if(mobilePackage.equalsIgnoreCase("a")) {
			if(minutes > 450) {
				minutesCost = (double)(minutes - 450) * .45;
			}
			charge = 39.99 + minutesCost;
		}
		if(mobilePackage.equalsIgnoreCase("b")) {
			if(minutes > 900) {
				minutesCost = (double)(minutes - 900) * .40;
			}
			charge = 59.99 + minutesCost;
		}
		if(mobilePackage.equalsIgnoreCase("c")) {
			charge = 69.99;
		}
		return "Your chosen mobile package will cost: $" + charge;
	}
}
