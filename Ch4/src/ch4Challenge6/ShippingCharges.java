package ch4Challenge6;

public class ShippingCharges {
	private int packageWeight;
	
	public void setWeight(int weight) {
		packageWeight = weight;
	}
	
	public String calcShipCharges() {
		double shippingCost = 0;
		if(packageWeight <= 2) {
			shippingCost = 1.1;
		}
		else if(packageWeight < 6) {
			shippingCost = 2.2;
		}
		else if(packageWeight < 10) {
			shippingCost = 3.7;
		}
		else if(packageWeight > 10) {
			shippingCost = 4.8;
		}
		
		return "The cost to ship this package will be: $" + shippingCost;
	}

}
