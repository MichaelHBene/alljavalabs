package ch4Challenge10;

public class FreezingBoiling {
	private double temperature;
	
	public void setTemperature(double enteredTemp) {
		temperature = enteredTemp;
	}
	
	public double getTemperature() {
		return temperature;
	}
	
	public boolean isEthylFreezing(){
		boolean isTemp = false;
		if(temperature <= -173) {
			isTemp = true;
		}
		return isTemp;
	}
	public boolean isEthylBoiling() {
		boolean isTemp = false;
		if(temperature >= 172) {
			isTemp = true;
		}
		return isTemp;
	}
	public boolean isWaterFreezing(){
		boolean isTemp = false;
		if(temperature <= 32) {
			isTemp = true;
		}
		return isTemp;
	}
	public boolean isWaterBoiling() {
		boolean isTemp = false;
		if(temperature >= 212) {
			isTemp = true;
		}
		return isTemp;
	}
	public boolean isOxygenFreezing(){
		boolean isTemp = false;
		if(temperature <= -362) {
			isTemp = true;
		}
		return isTemp;
	}
	public boolean isOxygenBoiling() {
		boolean isTemp = false;
		if(temperature >= -306) {
			isTemp = true;
		}
		return isTemp;
	}
	
	public String getList() {
		String tempList = "";
		if(this.isEthylFreezing()) {
			tempList +="Ethyl\n";
		}
		if(this.isEthylBoiling()) {
			tempList += "Ethyl\n";
		}
		if(this.isOxygenFreezing()) {
			tempList +="Oxygen\n";
		}
		if(this.isOxygenBoiling()) {
			tempList += "Oxygen\n";
		}
		if(this.isWaterFreezing()) {
			tempList +="Water\n";
		}
		if(this.isWaterBoiling()) {
			tempList += "Water\n";
		}
		return tempList;
	}
	
}
