package ch4Challenge2;

import java.util.Scanner;
public class TimeCalculator {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int remainingSeconds = 0;
		int minutes = 0;
		int hours = 0;
		int days = 0;
		System.out.println("Please enter the number of seconds:");
		Scanner keyboard = new Scanner (System.in);
		int secondsInput = keyboard.nextInt();
		
		if(secondsInput >= 60) {
			if(secondsInput >= 3600) {
				if(secondsInput >= 86400) {
					days = secondsInput / 86400;
					hours = (secondsInput % 86400) / 3600;
					minutes = (secondsInput - (hours * 3600) - (days * 86400)) / 60;
					remainingSeconds = secondsInput % 60;
				}
				else {
					hours = secondsInput / 3600;
					minutes = (secondsInput - (hours * 3600)) / 60;
					remainingSeconds = secondsInput % 60;	
				}
			}
			else {
				minutes = secondsInput / 60;
				remainingSeconds = secondsInput % 60;	
			}
		}
		else {
			remainingSeconds = secondsInput;
		}
		
		System.out.println(days + ":" + hours + ":" + minutes + ":" + remainingSeconds);
	}

}
