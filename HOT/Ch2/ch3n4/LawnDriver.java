package ch3n4;

import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the width for lawn 1:");
		double lawn1Width = keyboard.nextDouble();
		System.out.println("Please enter the length for lawn 1:");
		double lawn1Length = keyboard.nextDouble();
		
		Lawn lawn1 = new Lawn(lawn1Width, lawn1Length);
		
		Lawn lawn2 = new Lawn();
		
		System.out.println("Please enter the width for lawn 2:");
		lawn2.setWidth(keyboard.nextInt());
		System.out.println("Please enter the length for lawn 2:");
		lawn2.setLength(keyboard.nextInt());
		
		double lawn1SqF = lawn1.calcArea();
		double lawn2SqF = lawn2.calcArea();
		double lawnCost = 0;
		
		if(lawn1SqF < 600) {
			if(lawn1SqF < 400) {
				lawnCost = 20 * 25;
				System.out.printf("Lawn 1 weekly lawn cost: $%.2f\n", 25.0);
				System.out.printf("Lawn 1 total lawn cost: $%.2f\n", lawnCost);
			}
			else {
				lawnCost = 20 * 35;
				System.out.printf("Lawn 1 weekly lawn cost: $%.2f\n", 35.0);
				System.out.printf("Lawn 1 total lawn cost: $%.2f\n", lawnCost);
			}
		}
		else {
			lawnCost = 20 * 50;
			System.out.printf("Lawn 1 weekly lawn cost: $%.2f\n", 50.0);
			System.out.printf("Lawn 1 total lawn cost: $%.2f\n", lawnCost);
		}
		
		if(lawn2SqF < 600) {
			if(lawn2SqF < 400) {
				lawnCost = 20 * 25;
				System.out.printf("Lawn 2 weekly lawn cost: $%.2f\n", 25.0);
				System.out.printf("Lawn 2 total lawn cost: $%.2f\n", lawnCost);
			}
			else {
				lawnCost = 20 * 35;
				System.out.printf("Lawn 2 weekly lawn cost: $%.2f\n", 35.0);
				System.out.printf("Lawn 2 total lawn cost: $%,.2f\n", lawnCost);
			}
		}
		else {
			lawnCost = 20 * 50;
			System.out.printf("Lawn 2 weekly lawn cost: $%.2f\n", 50.0);
			System.out.printf("Lawn 2 total lawn cost: $%.2f\n", lawnCost);
		}
		
	}

}
