package Arrays;

import java.util.ArrayList;

public class Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Array
		int[] array1 = new int[5];
		int[] array2 = {4, 7, 8, 3, 1, 9, 0, 5};
		
		for(int x = 0; x < array1.length; ++x) {
			array1[x] = x + 1;
			System.out.println("Array index at " + x + ": " + array1[x]);
		}
		
		//ArrayList
		ArrayList arrayList = new ArrayList();
		
		int x = 1;
		
		arrayList.add(x);
		
		System.out.println("Array List at index 0: " + arrayList.get(0));
		
		//Sequential Search
		for(int i = 0; i < array1.length; ++i) {
			if(array1[i] == 3) {
				System.out.println("The number 3 has been found. It is at index " + i + ".");
				break;
			}
		}
		
		//Selection Sort
		int startScan, index, minIndex, minValue;
		for(startScan = 0; startScan < (array2.length -1); ++startScan) {
			minIndex = startScan;
			minValue = array2[startScan];
			for(index = startScan + 1; index < array2.length; ++index) {
				if(array2[index] < minValue) {
					minValue = array2[index];
					minIndex = index;
				}
			}
			array2[minIndex] = array2[startScan];
			array2[startScan] = minValue;
		}
		
		for(int z = 0; z < array2.length; ++ z) {
			System.out.print(array2[z] + " ");
		}
		
	}

}
