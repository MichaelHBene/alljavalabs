package Aggregation;

public class Test2 {
	//Aggregation
	private Test1 test1;
	
	public Test2(Test1 test) {
		test1 = test;
	}
	
	public Test1 getTest1() {
		return test1;
	}

}
