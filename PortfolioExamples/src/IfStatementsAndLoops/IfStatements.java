package IfStatementsAndLoops;

public class IfStatements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name1 = "mike";
		String name2 = "Mike";
		//Compare strings
		if(name1.equalsIgnoreCase(name2)) {
			System.out.println("Names are the same!");
		}
		
		//if statement
		if(5 > 3) {
			System.out.println("5 is greater than 3!");
		}
		
		//if-else statement
		if(5 < 3) {
			System.out.println("3 is greater than 5!");
		}
		else {
			System.out.println("3 is not greater than 5!");
		}
		
		//if-else-if statement
		if(10 == 11) {
			System.out.println("10 equals 11!");
		}else if(11 == 11) {
			System.out.println("11 equals 11!");
		}
		
		//switch statement
		int switcher = 1;
		
		switch(switcher) {
			case 1:{
				System.out.println("The switcher is 1!");
				break;
			}
			case 2:{
				System.out.println("The switcher is 2!");
				break;
			}
			case 3:{
				System.out.println("The switcher is 3!");
				break;
			}
			
		}

	}

}
