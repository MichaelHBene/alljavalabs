package IfStatementsAndLoops;

import java.util.*;
public class Loops {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		//running total variable
		int runningTotal = 0;
		//for loop
		for(int i = 0; i < 3; ++i) {
			++runningTotal;
			System.out.println("counter" + i + " runningTotal: " + runningTotal);
		}
		
		//while loop
		System.out.println("Enter a number, enter 0 to end the loop");
		int input = keyboard.nextInt();
		//sentinel value
		while(input != 0) {
			System.out.println("Enter a number, enter 0 to end the loop");
			input = keyboard.nextInt();
		}
		
		//do-while loop
		int counter2 = 0;
		do {
			System.out.println("Counter is " + counter2);
			++counter2;
		}
		while(counter2 < 3);	
		

	}

}
