package Exceptions;

public class ExceptionDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			int x = 1;
			int y = 0;
			
			if(x > y) {
				//throwing exceptions
				throw new TestException();
			}
			
		}
		catch(TestException e) {
			//handling exceptions
			System.out.println(e.getMessage());
		}
		

	}

}
