package Exceptions;

//Extending a parent class
public class TestException extends Exception{
	public TestException() {
		super("Test exception has been thrown.");	
	}
}
