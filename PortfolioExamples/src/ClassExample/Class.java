package ClassExample;

public class Class {
	//Instance field
	private int number;
	private double number2;
	
	//Constructor
	public Class() {
		
	}
	//Overloaded Constructor
	public Class(int num) {
		number = num;
	}
	
	//Instance method
	public void setNumber(int num) {
		number = num;
	}
	//Overloaded method
	public void setNumber(double num) {
		number2 = num;
	}
	
	public int getNumber1() {
		return number;
	}
	public double getNumber2() {
		return number2;
	}
	
	//Overridden toString()
	public String toString() {
		//this reference variable
		return "The first number is: " + this.number;
	}
	
	//Overridden equals
	public Boolean equals() {
		if(number != number2) {
			return false;
		}
		else {
			return true;
		}
	}

}
