package GUIs;

import javafx.scene.control.*;
import javafx.application.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Buttons extends Application{
	
	private Button button1 = new Button("Button 1");
	private Button button2 = new Button("Button 2");
	private Button button3 = new Button("Button 3");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
		HBox buttonDisplay = new HBox(5, button1, button2, button3);
		Scene scene = new Scene(buttonDisplay);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Buttons");
		primaryStage.show();
		
	}

}
