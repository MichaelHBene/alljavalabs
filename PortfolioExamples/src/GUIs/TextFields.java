package GUIs;

import javafx.application.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TextFields extends Application{
	
	private TextField textfield1 = new TextField();
	private TextField textfield2 = new TextField();
	private TextField textfield3 = new TextField();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
		VBox textFieldDisplay = new VBox(5, textfield1, textfield2, textfield3);
		Scene scene = new Scene(textFieldDisplay);
		primaryStage.setScene(scene);
		primaryStage.setTitle("TextFields");
		primaryStage.show();
		
	}

}
