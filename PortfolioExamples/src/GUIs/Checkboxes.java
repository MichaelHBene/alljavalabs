package GUIs;

import javafx.application.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Checkboxes extends Application{

	private CheckBox check1 = new CheckBox();
	private CheckBox check2 = new CheckBox();
	private CheckBox check3 = new CheckBox();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
		VBox textFieldDisplay = new VBox(5, check1, check2, check3);
		Scene scene = new Scene(textFieldDisplay);
		primaryStage.setScene(scene);
		primaryStage.setTitle("TextFields");
		primaryStage.show();
		
	}

}
