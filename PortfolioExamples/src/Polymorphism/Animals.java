package Polymorphism;

public class Animals {
	
	protected String type;

	//Static class member
	public static String printStatement() {
		return "This is a test of a static class member!";
	}
	
	
	public String speak() {
	
		return("The animal is a " + type);
		
	}

	
	

}
