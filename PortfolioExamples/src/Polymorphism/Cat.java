package Polymorphism;

public class Cat extends Animals{
	
	public Cat() {
		super.type = "Cat";
	}
	
	public String speak() {
		System.out.println("The animal says meow!");
		return super.speak();
	}

}
