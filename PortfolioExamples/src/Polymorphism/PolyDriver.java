package Polymorphism;

public class PolyDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Polymorphism
		Animals dog = new Dog();
		Animals cat = new Cat();
		
		//Static class method
		System.out.println(Animals.printStatement());
		
		System.out.println(dog.speak());
		System.out.println(cat.speak());
		

	}

}
