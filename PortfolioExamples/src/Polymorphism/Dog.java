package Polymorphism;

public class Dog extends Animals{
	
	public Dog() {
		super.type = "Dog";
	}
	
	public String speak() {
		System.out.println("The animal says bark!");
		return super.speak();
	}

}
